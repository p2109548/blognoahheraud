<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConferenceController extends AbstractController
{
    #[Route('/', name: 'app_conference')]
    
    public function index(ArticleRepository $varArt, CategorieRepository $varCat): Response
    {
        $articles = $varArt->findAll();
        $categories = $varCat->findAll();
        return $this->render('base/index.html.twig', [
            'controller_name' => 'ConferenceController',
            'articles' => $articles,
            'categories' => $categories,
        ]);
    }
    #[Route('/{id}', name: 'article_show', methods: ['GET'])]
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', [
            'article' => $article,
        ]);
    }
}
